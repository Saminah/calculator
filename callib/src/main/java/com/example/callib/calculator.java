package com.example.callib;

public class calculator {
    private double num1,num2;
    private String operator;


  // ****************   Getter and Setter NUM1   *******************
    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    // ****************   Getter and Setter NUM2   *******************

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }
// ****************   Getter and Setter Operator  *******************

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;


    }
//****************************************************************************
    double resault(){
        double hasel;
        switch (operator) {
            case "+":
                        hasel= num1 + num2;
                        break;
            case "*":
                        hasel=num1*num2;
                        break;
            case "-":
                        hasel=num1-num2;
                        break;
            case "/":
                        hasel=num1/num2;
                        break;
            case "^":
                       hasel=tavan(num1,num2);
                       break;
            case "sqrt":
                      hasel=jazr(num1);
                      break;
            case "sin":
                      hasel=Math.round(Math.sin(num1));
                      break;
            case "cos":
                      hasel=Math.round(Math.cos(num1));
                      break;
            case "tan":
                      hasel=Math.sin(num1)/Math.cos(num1);
                      break;
            case "fac":
                      hasel=factoriel(num1);
                      break;

            default:
                       hasel=0;
        }
        return hasel;
    }


    double tavan(double x,double y){
        return Math.pow(x,y);
    }

    double jazr(double x){
        return Math.sqrt(x);
    }
    double factoriel(double x){
        double fac=1;
        int i;
            for(i=1;i<=x;i++) {
              fac=fac*i;
        }
        return fac;
    }

}

